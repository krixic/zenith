import { api } from "../../../api/api";
import { MetadataUtil } from "../../../util/MetadataUtil";
import { APIError } from "../../../util/errors/APIError";

interface PageParams {
    params: {
        position: string
    }
}

export async function generateMetadata({ params }: PageParams) {
    const e = await api.challenge.fetch.position(undefined, {
        position: params.position
    });

    return MetadataUtil.getMetadataForPage(`Challenge ${params.position}`, {

    });
}

export default async function Page({ params }: PageParams) {
    const challenge = await api.challenge.fetch.position(undefined, {
        position: params.position
    });

    if (challenge.err) {
        throw new APIError(challenge.val);
    }

    return (
        <div>
            <h1>Challenge {params.position}</h1>
        </div>
    )
}