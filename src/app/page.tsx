import { MetadataUtil } from "../util/MetadataUtil"

export const metadata = MetadataUtil.getMetadataForPage(
    "Home"
)

export default function Page() {
    return (
        <>
            <div className="absolute w-full h-44 inset-0 -z-50" style={{ backgroundColor: "black"}}>

            </div>
            <h1>Home</h1>
        </>
    )
}