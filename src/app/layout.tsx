import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "../styles/app.scss";
import Navbar from "../components/Navbar";
import { ErrorBoundary } from "next/dist/client/components/error-boundary";
import Error from "./error";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${inter.className}`}>
        <div id="app">
          <Navbar />
          <div className="flex mt-40 justify-center">
            {children}
          </div>
        </div>
      </body>
    </html>
  );
}
