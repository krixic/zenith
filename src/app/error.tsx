"use client";

import ErrorTemplate from "../components/errors/ErrorTemplate";
import { APIError } from "../util/errors/APIError";

export default function Error({
    error,
    reset
}: { 
    error: Error & { digest?: string },
    reset: () => void
}) {
    return (
        <ErrorTemplate code={parseInt(error.message.split(": ")[0])} message={error.message} />
    )
}