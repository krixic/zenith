import { z } from "zod";

export const group = z.object({
    id: z.string(),
    name: z.string(),
    priority: z.number(),
    color: z.string()
});

export type Group = z.infer<typeof group>;
