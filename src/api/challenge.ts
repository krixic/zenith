import { createProcedure } from "./router";
import apiDefaultResponse from "./apiDefaultResponse";
import { z } from "zod";

export const player = z.object({
    id: z.string(),
    name: z.string(),
    banned: z.boolean()
});

export type Player = z.infer<typeof player>;

const recordChallenge = z.object({
    id: z.string(),
    name: z.string(),
    position: z.number(),
    video: z.string(),
    level_id: z.optional(z.string()),
})

export const record = z.object({
    id: z.string(),
    submitted_at: z.date(),
    updated_at: z.date(),
    video: z.string(),
    status: z.number(), // todo : enum
    type: z.number(), // todo : ^
    challenge: recordChallenge,
    player: player
});

export type Record = z.infer<typeof record>;

export const challenge = z.object({
    id: z.string(),
    name: z.string(),
    position: z.number(),
    video: z.string(),
    level_id: z.optional(z.string()),
    verifier: z.optional(player),
    publisher: z.optional(player),
    creators: z.optional(z.array(player)),
    records: z.optional(z.array(record))
});

export type Challenge = z.infer<typeof challenge>;

export enum ChallengeType {
    Main = "main",
    Legacy = "legacy"
}

export const challengeRouter = {
    crud: {
        create: createProcedure()
            .meta({ path: "/challenges/", method: "POST" })
            .input(z.object({
                name: z.string(),
                position: z.number(),
                verifier: z.string(),
                creators: z.array(z.string()),
                publisher: z.string(),
                video: z.string(),
                fps: z.optional(z.string())
            }))
            .output(apiDefaultResponse(challenge)),
        patch: createProcedure()
            .meta({ path: "/challenges/:id", method: "PATCH" })
            .input(z.object({
                name: z.optional(z.string()),
                position: z.optional(z.string())
            }))
            .output(apiDefaultResponse(challenge))
    },
    fetch: {
        list: createProcedure()
            .meta({ path: "/challenges/list", method: "GET" })
            .input(z.object({
                type: z.nativeEnum(ChallengeType)
            }))
            // todo
            .output(apiDefaultResponse(z.object({

            }))),
        position: createProcedure()
            .meta({ path: "/challenges/list/:position", method: "GET" })
            .output(apiDefaultResponse(challenge)),
        index: createProcedure()
            .meta({ path: "/challenges/", method: "GET" })
            // todo : pagination
            // too lazy to change typ
            // @ts-ignore
            .output(apiDefaultResponse(z.array(challenge)))
    }
}
