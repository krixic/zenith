import { Ok, Err, Some } from "ts-results";
import type { Result } from "ts-results";
import { createClient, createRequest } from "./client";
import type { HTTPRequest } from "./client";
import { z } from "zod";

export type ProcedureMeta = {
    path: string;
    method: "GET" | "POST" | "PUT" | "DELETE" | "PATCH";
}

export type HTTPError = {
    code: number;
    message: string;
    data?: string;
}

export interface Procedure<TIn = any, TOut = Result<any, HTTPError>> {
    _input?: TIn;
    _output?: TOut;
    _parseInput?: (input: TIn) => TIn;
    _parseOutput?: (input: TOut) => TOut;
    _mutateResponse?: <T = TOut>(request: HTTPRequest<TIn, T>) => TOut;
    _meta: ProcedureMeta;
    _client: ReturnType<typeof createClient>
    _cacheMethod: RequestCache

    (args?: TIn): Promise<TOut>
    (args?: TIn, query?: object): Promise<TOut>
    input<T extends z.ZodSchema<any, any>>(schema: T): Procedure<z.infer<typeof schema>, TOut>;
    output<T extends z.ZodSchema<any, any>>(schema: T): Procedure<TIn, Result<z.infer<typeof schema>, HTTPError>>;
    mutateResponse<T = TOut>(callback: (request: HTTPRequest<TIn, T>) => TOut): Procedure<TIn, TOut>
    meta(meta: ProcedureMeta): Procedure<TIn, TOut>;
    cache(method: RequestCache): Procedure<TIn, TOut>;
}

export function createProcedure(): Procedure {
    const procedure: any = (arg: any, query?: any) => {
        return new Promise(async (res, rej) => {
            procedure._parseInput?.(arg);
            let request = createRequest(procedure, arg, query);
            await procedure._client.perform(request);

            if (request.result.some) {
                if (request.result.val.ok) {
                    let output = Ok(JSON.parse(request.result.val.val));
                    request.result = Some(output);

                    if (procedure._client.extensions) {
                        for (let extension of procedure._client.extensions) {
                            extension.BeforeMutation?.(request);
                        }
                    }

                    if (procedure._mutateResponse)
                        output = procedure._mutateResponse(request);

                    if (procedure._client.extensions) {
                        for (let extension of procedure._client.extensions) {
                            extension.AfterMutation?.(request);
                        }
                    }

                    if (procedure._parseOutput) {
                        if (output.ok) {
                            output = Ok(procedure._parseOutput(output.val))
                        }
                    }

                    return res(output);
                } else {
                    return res(Err(request.result.val.val));
                }
            } else {
                rej("No response from server");
            }
        });
    };

    procedure.input = <T extends z.ZodSchema<any, any>>(schema: T) => {
        procedure._input = {} satisfies z.infer<typeof schema>;
        procedure._parseInput = (input: z.infer<typeof schema>) => { return schema.parse(input) as z.infer<typeof schema> }
        return procedure as Procedure<z.infer<typeof schema>, typeof procedure["_output"]>;
    };
    procedure.output = <T extends z.ZodSchema<any, any>>(schema: T) => {
        procedure._output = {} satisfies z.infer<typeof schema>;
        procedure._parseOutput = (input: z.infer<typeof schema>) => {
            return schema.parse(schema, input) as z.infer<typeof schema>
        }
        return procedure as Procedure<typeof procedure["_input"], Result<z.infer<typeof schema>, HTTPError>>;
    };
    procedure.mutateResponse = <T = typeof procedure["_output"]>(callback: (request: HTTPRequest<typeof procedure["_input"], T>) => typeof procedure["_output"]) => {
        procedure._mutateResponse = callback;
        return procedure as Procedure<typeof procedure["_input"], typeof procedure["_output"]>;
    }
    procedure.meta = (meta: any) => {
        procedure._meta = meta;
        return procedure;
    };
    procedure.cache = (method: any) => {
        procedure._cacheMethod = method;
        return procedure;
    };

    return procedure satisfies Procedure;
}
