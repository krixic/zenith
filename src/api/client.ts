import { None, Ok, Err, Some } from "ts-results";
import type { Result, Option } from "ts-results";
import type { HTTPError, Procedure } from "./router";
import { cookies } from "next/headers";

export type HTTPRequest<TIn = any, TOut = any> = {
    path: string;
    fullPath: string;
    parameters?: object;
    finalPath: string;
    method: "GET" | "POST" | "PUT" | "DELETE" | "PATCH";
    cacheMethod: RequestCache;
    input: Option<TIn>;
    result: Option<Result<TOut, HTTPError>>;
}

export interface Extension {
    beforeRequest?: (req: HTTPRequest) => void;
    afterRequest?: (req: HTTPRequest) => void;
    beforeMutation?: (req: HTTPRequest) => void;
    afterMutation?: (req: HTTPRequest) => void;
}

type Router = Procedure | {
    [key: string]: Router
}

type ClientOptions = {
    baseUrl: string;
    routes: Router[],
    extensions?: Extension[]
}

export function createRequest<T extends Procedure>(procedure: T, args?: T["_input"], query?: object): HTTPRequest<T["_input"], T["_output"]> {
    const meta = procedure._meta;
    const path = meta.path;
    const regex = new RegExp(/:([^\/]*)+/g);

    let finalPath = `${meta.path}`

    if (regex.test(path)) {
        const matches = path.match(regex);

        if (matches != null) {
            if (!query) {
                throw new Error("Query parameters must be provided with this route.");
            }

            for (const group of matches ?? []) {
                const param = group.substring(1)
                // @ts-ignore
                finalPath = finalPath.replace(group, encodeURIComponent(query[param]));
            }
        }
    }

    return {
        path: meta.path,
        method: meta.method,
        fullPath: `${procedure._client.baseUrl}${meta.path}`,
        parameters: query,
        finalPath: `${procedure._client.baseUrl}${finalPath}`,
        input: args === undefined ? None : Some(args),
        result: None,
        cacheMethod: procedure._cacheMethod ?? "default"
    }
}

export function createClient(options: ClientOptions) {
    const client = {
        ...options,
        perform: async (httpRequest: HTTPRequest) => {
            if (client.extensions) {
                for (let extension of client.extensions) {
                    extension.beforeRequest?.(httpRequest);
                }
            }

            const response = await fetch(`${httpRequest.finalPath}`, {
                body: httpRequest.input.some ? JSON.stringify(httpRequest.input.val) : undefined,
                method: httpRequest.method,
                cache: httpRequest.cacheMethod,
                mode: 'cors',
                headers: { 
                    'Content-Type': 'application/json',
                    'Cookie': cookies().toString()
                }
            });

            if (response.status === 200) {
                httpRequest.result = Some(new Ok(await response.text()));
            } else {
                let json = await response.json();
                let error = { code: response.status, data: json.data, message: json.message } satisfies HTTPError;
                httpRequest.result = Some(new Err(error));
            }

            if (client.extensions) {
                for (let extension of client.extensions) {
                    extension.afterRequest?.(httpRequest);
                }
            }
        }
    };

    function bindClientToRoutes(routes: Router) {
        let keys = Object.keys(routes);

        for (let key of keys) {
            // @ts-ignore
            if (typeof routes[key] === "function") {
                // @ts-ignore
                routes[key]._client = client;
                // @ts-ignore
            } else if (typeof routes[key] === "object") {
                // @ts-ignore
                bindClientToRoutes(routes[key])
            }
        }
    }

    for (let route of client.routes)
        bindClientToRoutes(route);

    return client;
}