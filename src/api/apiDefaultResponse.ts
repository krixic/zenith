import { z } from "zod";

export default function apiDefaultResponse<T extends any>(data: z.ZodSchema<T>) {
    return z.object({
        status: z.number(),
        message: z.optional(z.string()),
        data: data
    });
}