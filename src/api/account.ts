import { createProcedure } from "./router";
import { group } from "./group";
import apiDefaultResponse from "./apiDefaultResponse";
import { z } from "zod";

export const account = z.object({
    id: z.string(),
    username: z.string(),
    created_at: z.date(),
    updated_at: z.date(),
    country_code: z.optional(z.string()),
    subdivision_code: z.optional(z.string()),
    flags: z.number(),
    groups: z.array(group)
});

export type Account = z.infer<typeof account>;

export const session = z.object({
    id: z.string(),
    token: z.string(),
    created_at: z.date(),
    updated_at: z.date(),
    account: account
});

export type Session = z.infer<typeof session>;

export const accountRouter = {
    authorization: {
        register: createProcedure()
            .meta({ path: "/authorization/register", method: "POST" })
            .input(z.object({
                username: z.string(),
                password: z.string()
            }))
            .output(apiDefaultResponse(account)),
        login: createProcedure()
            .meta({ path: "/authorization/login", method: "POST" })
            .input(z.object({
                username: z.string(),
                password: z.string()
            }))
            .output(apiDefaultResponse(session)),
        // todo : logout event for the API?
    },
    getSelf: createProcedure()
        .meta({ path: "/accounts/self", method: "GET" })
        // todo : .authorized()
}