import { createClient } from "./client";
import { accountRouter } from "./account";
import { challengeRouter } from "./challenge";

function getBaseUrl() {
    if (typeof window !== "undefined") return "";

    if (process.env.NODE_ENV === "production") return "https://next.challengelist.gd"
    return `http://localhost:${process.env.API_PORT ?? 3001}`;
}

createClient({
    baseUrl: getBaseUrl(),
    routes: [
        accountRouter,
        challengeRouter
    ],
    extensions: [
        {
            beforeRequest(req) {
                console.log(`Requesting to ${req.finalPath} ${req.input.some ? `(${JSON.stringify(req.input.val)})` : ""}`);
            },
        }
    ]
});

export const api = {
    account: accountRouter,
    challenge: challengeRouter
}
