export default function ErrorTemplate({ code, message }: { code: number, message: string }) {
    return (
        <>
            <h1>Error {code}</h1>
            <p>{message}</p>
        </>
    )
}