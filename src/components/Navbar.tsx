"use client";

import "./navbar.scss";
import { usePathname } from "next/navigation";
import { Montserrat } from "next/font/google";
import Link from "next/link";

const montserrat = Montserrat({ subsets: ["latin"], weight: "600" });

export default function Navbar() {
    const pathname = usePathname();
    const isHome = pathname === "/";

    return (
        <div className={`absolute inset-0 flex h-40 justify-center px-4 ${isHome ? "_home" : ""}`}>
            <div className="flex flex-row h-10 mt-2 justify-between items-center max-w-5xl w-full bg-col-[9%] rounded-md px-6 shadow-lg">
                <div className="flex items-center h-full">
                    <h1 className={`${montserrat.className}`}>
                        <Link href="/">Challenge List</Link>
                    </h1>
                    <span className="w-[3px] h-3/6 ml-4 bg-col-[17%] rounded-full"></span>
                    <Link href="/test" className="ml-4">Test</Link>
                </div>
                <div className="flex gap-4 items-center h-full txt-col-[40%]">
                    <a href="#">Sign in</a>
                    <a href="#">Sign up</a>
                </div>
            </div>
        </div>
    )
}
