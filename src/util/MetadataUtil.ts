import { Metadata } from "next";

export class MetadataUtil {
    public static getMetadataForPage(page: string, ref?: Metadata): Metadata {
        return {
            title: `Challenge List - ${page}`,
            description: ref?.description ?? "The",
            openGraph: {
                type: "website",
                title: `Challenge List - ${page}`,
                description: "The",
            },
            themeColor: "#6499f5"
        }
    }
}